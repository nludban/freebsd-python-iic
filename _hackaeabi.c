/*
 * pybsd/compat_aeabi.c
 *
 * XXX Copyright + License...
 */

#include <Python.h>

/*********************************************************************/

//ImportError: /usr/local/lib/libblas.so.2:
//	Undefined symbol "__aeabi_uidiv"

//$ nm -n _hackaeabi.so
//00000558 T __aeabi_uidiv

unsigned int
__aeabi_uidiv(
   unsigned int		n,
   unsigned int		d
   )
{
   return 42;
}

/*********************************************************************/

static struct PyModuleDef hackaeabi_module =
{
   PyModuleDef_HEAD_INIT,
   "_hackaeabi",
   NULL,		// m_doc
   -1,			// m_size
   NULL,		// m_methods
   0			// ...
};


PyObject *
PyInit__hackaeabi(void)
{
   PyObject *m = PyModule_Create(&hackaeabi_module);
   if (m == NULL)
      return NULL;

   return m;
}

/**/
