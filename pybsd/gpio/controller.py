#!python3.6
# pybsd/gpio/controller.py

import _bsdgpio

from .pin import Pin

import os

#---------------------------------------------------------------------#

class Controller:

    def __init__(self, dev_name=None):
        self._name = None
        self._fdesc = None
        self._pins = None
        self._listed = False
        if dev_name is not None:
            self.open(dev_name)
        return

    def __del__(self):
        self.close()
        return

    def close(self):
        if self._fdesc is not None:
            self._fdesc, fdesc = None, self._fdesc
            os.close(fdesc)
        return

    def open(self, dev_name):
        self._fdesc = os.open(dev_name, os.O_RDWR)
        self._name = dev_name.split('/')[-1]
        self._pins = [ None ] * _bsdgpio.maxpin(self._fdesc)
        return

    def list_pins(self, *, force=False):
        if force or not self._listed:
            for k in range(len(self._pins)):
                if self._pins[k] is None:
                    self._pins[k] = Pin(self, k)
                elif force:
                    self._pins[k].read_config()
            self._listed = True
        return tuple(self._pins)

    def get_pin(self, *, number=None, name=None):
        if number is not None:
            number = int(number)
            if self._pins[number] is None:
                self._pins[number] = Pin(self, number)
            return self._pins[number]
        if name is not None:
            name = str(name)
            for p in self.list_pins():
                if p.name == name:
                    return p
            raise KeyError('no such pin (name=%s)' % name)
        raise ValueError('pin name or number is required')

#--#
