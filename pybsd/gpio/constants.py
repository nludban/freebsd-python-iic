#!python3.6

import _bsdgpio

LOW		= False	#_bsdgpio.PIN_LOW
HIGH		= True	#_bsdgpio.PIN_HIGH

INPUT		= _bsdgpio.PIN_INPUT

OUTPUT		= _bsdgpio.PIN_OUTPUT
OPENDRAIN	= _bsdgpio.PIN_OPENDRAIN
PUSHPULL	= _bsdgpio.PIN_PUSHPULL
TRISTATE	= _bsdgpio.PIN_TRISTATE

PULLUP		= _bsdgpio.PIN_PULLUP
PULLDOWN	= _bsdgpio.PIN_PULLDOWN

INVIN		= _bsdgpio.PIN_INVIN
INVOUT		= _bsdgpio.PIN_INVOUT

#PULSATE	= _bsdgpio.PIN_PULSATE

#--#
