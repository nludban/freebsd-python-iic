#!python3.6
#
# pybsd/gpio/__init__.py
#

from .constants import *

from .controller import Controller

def open(device=0):
    if isinstance(device, int):
        device = '/dev/gpioc%d' % device
    return Controller(device)

#--#
