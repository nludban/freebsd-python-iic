#!python3.6
# pybsd/gpio/pin.py

import contextlib

import _bsdgpio

from .constants import *

#---------------------------------------------------------------------#

class Flags:

    def __init__(self, flags):
        self._flags = flags
        return

    @property
    def input(self):
        return ((self._flags & INPUT) != 0)

    @property
    def output(self):
        return ((self._flags & OUTPUT) != 0)

    @property
    def direction(self):
        # Precedence takeen from bcm2835_gpio.c:
        if self.output: return OUTPUT
        if self.input: return INPUT
        return None

    @property
    def opendrain(self):
        return ((self._flags & OPENDRAIN) != 0)

    @property
    def pushpull(self):
        return ((self._flags & PUSHPULL) != 0)

    @property
    def tristate(self):
        return ((self._flags & TRISTATE) != 0)

    @property
    def driver(self):
        if self.opendrain(): return OPENDRAIN
        if self.pushpull(): return PUSHPULL
        if self.tristate(): return TRISTATE
        return None

    @property
    def pullup(self):
        return ((self._flags & PULLUP) != 0)

    @property
    def pulldown(self):
        return ((self._flags & PULLDOWN) != 0)

    @property
    def pull(self):
        # Precedence takeen from bcm2835_gpio.c:
        if self.pullup: return PULLUP
        if self.pulldown: return PULLDOWN
        return None

    @property
    def invin(self):
        return ((self._flags & INVIN) != 0)

    @property
    def invout(self):
        return ((self._flags & INVOUT) != 0)


class Capabilities(Flags):

    def __init__(self, flags):
        FlagBits.__init__(self, flags)
        return


class Configuration(Flags):

    def __init__(self, flags):
        Flags.__init__(self, flags)
        self._initial = None
        return

    def _set_bits(self, yes_no, bits, mask):
        if yes_no:
            self._flags &= ~mask
            self._flags |= bits
        else:
            self._flags &= ~bits
        return

    @Flags.input.setter
    def input(self, yes_no):
        return self._set_bits(yes_no, INPUT, INPUT|OUTPUT)

    @Flags.output.setter
    def output(self, yes_no):
        return self._set_bits(yes_no, OUTPUT, INPUT|OUTPUT)

    @Flags.direction.setter
    def direction(self, d):
        if d == INPUT: self.input = True
        elif d == OUTPUT: self.output = True
        elif d is None: self._flags &= ~(INPUT|OUTPUT)
        else: raise ValueError('direction must be INPUT|OUTPUT')

    @Flags.opendrain.setter
    def opendrain(self, yes_no):
        return self._set_bits(yes_no, OPENDRAIN,
                              OPENDRAIN|PUSHPULL|TRISTATE)

    @Flags.pushpull.setter
    def pushpull(self, yes_no):
        return self._set_bits(yes_no, PUSHPULL,
                              OPENDRAIN|PUSHPULL|TRISTATE)

    @Flags.tristate.setter
    def tristate(self, yes_no):
        return self._set_bits(yes_no, TRISTATE,
                              OPENDRAIN|PUSHPULL|TRISTATE)

    @Flags.driver.setter
    def driver(self, d):
        if d == OPENDRAIN: self.opendrain = True
        elif d == PUSHPULL: self.pushpull = True
        elif d == TRISTATE: self.tristate = True
        elif d is None: self._flags &= ~(OPENDRAIN|PUSHPULL|TRISTATE)
        else: raise ValueError(
                'driver must be OPENDRAIN|PUSHPULL|TRISTATE')

    @Flags.pullup.setter
    def pullup(self, yes_no):
        return self._set_bits(yes_no, PULLUP, PULLUP|PULLDOWN)

    @Flags.pulldown.setter
    def pulldown(self, yes_no):
        return self._set_bits(yes_no, PULLDOWN, PULLUP|PULLDOWN)

    @Flags.pull.setter
    def pull(self, d):
        if d == PULLUP: self.pullup = True
        elif d == PULLDOWN: self.pulldown = True
        elif d is None: self._flags &= ~(PULLUP|PULLDOWN)
        else: raise ValueError('pull must be PULLUP|PULLDOWN|None')

    @Flags.invin.setter
    def invin(self, yes_no):
        if yes_no:
            self._flags |= INVIN
        else:
            self._flags &= ~INVIN
        return

    @Flags.invout.setter
    def invout(self, yes_no):
        if yes_no:
            self._flags |= INVOUT
        else:
            self._flags &= ~INVOUT
        return

    @property
    def initial(self):
        return self._initial

    @initial.setter
    def initial(self, value):
        self._initial = (None if value is None else bool(value))


class Pin:

    def __init__(self, controller, pin_number):
        self._controller_name = controller._name
        self._fdesc = controller._fdesc
        self._number = pin_number
        self.read_config()
        return

    def __repr__(self):
        return '<%s.pin%i (%s)>' % ( self._controller_name,
                                     self._number,
                                     self._name )

    @property
    def number(self):
        return self._number

    def read_config(self):
        """
        Reload the pin configuration.  This is done once
        automatically when the Pin is created, can be repeated
        manually as needed to keep in synch with other gpio device
        users.
        """
        cfg = _bsdgpio.get_pin_config(self._fdesc, self._number)
        self._name = cfg[0].decode('utf-8')
        self._caps = cfg[1]
        self._flags = cfg[2]
        return

    @property
    def name(self):
        return self._name

    def set_name(self, name):
        bname = name.encode('utf-8')
        _bsdgpio.set_pin_name(self._fdesc, self._number, bname)
        self._name = name
        return

    def caps(self):
        """
        Get the Capabilities for the Pin.
        """
        return Capabilities(self._caps)

    def flags(self):
        """
        Get the (read-only) Flags for the Pin.
        """
        return Flags(self._flags)

    def _write_flags(self, flags):
        _bsdgpio.set_pin_flags(self._fdesc, self._number, flags)
        self._flags = flags
        return

    @contextlib.contextmanager
    def configure(self):
        """
        Create a context manager for configuring the Pin.

        Example:

        with pin.configure() as pc:
            pc.output = True
            pc.driver = pybsd.gpio.PUSHPULL
            pc.initial = 1
        """
        pc = Configuration(self._flags)
        yield pc
        if pc.output and pc._initial is not None:
            self.set(pc._initial)
        self._write_flags(pc._flags)
        return

    def get(self):
        return _bsdgpio.get_pin_value(self._fdesc, self._number)

    def set(self, value):
        return _bsdgpio.set_pin_value(self._fdesc, self._number,
                                      bool(value))

    def toggle(self):
        return _bsdgpio.toggle_pin(self._fdesc, self._number)

#--#
