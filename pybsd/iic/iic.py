#!python3.6

import os

import _bsdiic

#---------------------------------------------------------------------#

class IIC:

    def __init__(self, dev_name=None, slave_address=None):
        self._fdesc = None
        self._slave_address = None
        if dev_name is not None:
            self.open(dev_name, slave_address)
        return

    def __del__(self):
        self.close()
        return

    def close(self):
        if self._fdesc is not None:
            self._fdesc, fdesc = None, self._fdesc
            os.close(fdesc)
        return

    def open(self, dev_name, slave_address=None):
        self._fdesc = os.open(dev_name, os.O_RDWR)
        if slave_address is not None:
            self.set_slave_address(slave_address)
        return

    def set_slave_address(self, slave_address):
        if not (0 <= slave_address < 0x78):
            # 0x78 = 11110xx = 10-bit slave address prefix.
            # First byte of data could be rest of address?
            raise ValueError('invalid 7-bit slave address')
        #_bsdiic.set_slave_address(self._fdesc, slave_address)
        self._slave_address = slave_address
        return

    def write(self, data, *, slave_address=None):
        if not isinstance(data, bytes):
            raise TypeError('data must be of type bytes')
        if slave_address is None:
            slave_address = self._slave_address
        _bsdiic.write(self._fdesc, slave_address, data)
        return

    def read(self, nbytes, *, slave_address=None):
        if slave_address is None:
            slave_address = self._slave_address

        return os.read(self._fdesc, nbytes)

#--#
