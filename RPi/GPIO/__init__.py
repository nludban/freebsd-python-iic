#!python3.6
#
# RPi.GPIO.__init__.py
#


BCM		= 'BCM'
#BOARD		= 'BOARD'

INPUT		= 'INPUT'
OUTPUT		= 'OUTPUT'

LOW		= 0
HIGH		= 1

FALLING_EDGE	= 'FALLING'
RISING_EDGE	= 'RISING'
BOTH_EDGE	= 'BOTH'

PUD_OFF		= 'PUD_OFF'	# Off=disabled.
PUD_UP		= 'PUD_UP'	# Pull-up.
PUD_DOWN	= 'PUD_DOWN'	# Pull-down.

#functions: INPUT, OUTPUT, I2C, SPI, PWM, SERIAL, ?

def setup(channel, direction, pull_up_down=PUD_OFF, initial=None):
    pass

def cleanup(channel=None):
    pass

def output(chanlist, valuelist):
    pass

def input(channel):
    pass

def setmode(mode):
    if new_mode is not BCM:
        raise ValueError('Only BCM mode is supported.')

def getmode():
    return BCM

def add_event_detect(gpio, edge, callback=None, bouncetime=None):
    XXX

def remove_event_detect(channel):
    XXX

def event_detected(channel):
    XXX #return value

def add_event_callback(gpio, callback):
    XXX

def wait_for_edge(channel, edge, bouncetime=None, timeout=None):
    XXX #return channel

def gpio_function(channel):
    XXX

def setwarnings(yes_no):
    pass

#--#
