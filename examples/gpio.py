#!python3.6
#
# pybsd/examples/gpio.py
#

import pybsd.gpio

import time

c = pybsd.gpio.open('/dev/gpioc0')
for p in c.list_pins():
    print(p)

# Verified with logic analyzer at RPi-2 40-pin header.
bcm2 = c.get_pin(number=2)	# (i2c SDA / header pin 3)
bcm3 = c.get_pin(name='pin 3')	# (i2c SCL / header pin 5)

with bcm2.configure() as pc:
    pc.output = True
    pc.initial = 0

with bcm3.configure() as pc:
    pc.output = True
    pc.initial = 0

try:
    while True:
        # Loop runs at 10.7 kHz.
        bcm2.set(1)		# 1,0
        bcm3.set(1)		# 1,1
        bcm2.set(0)		# 0,1
        bcm3.set(0)		# 0,0
except KeyboardInterrupt:
    pass



# Verified with logic analyzer at RPi-2 40-pin header.
bcm12 = c.get_pin(number=12)	# (PWM 0 / header pin 32)
bcm25 = c.get_pin(name='pin 25') # (? / header pin 22)

with bcm12.configure() as pc:
    pc.output = True
    pc.initial = 0

with bcm25.configure() as pc:
    pc.output = True
    pc.initial = 0

try:
    while True:
        bcm12.set(1)		# 1,0
        bcm25.set(1)		# 1,1
        bcm12.set(0)		# 0,1
        bcm25.set(0)		# 0,0
        time.sleep(0.25)
except KeyboardInterrupt:
    pass


#--#
