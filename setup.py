#!/usr/bin/env python3.6

from distutils.core import setup, Extension

bsdiic_module = Extension('_bsdiic',
                          sources=['_bsdiic.c'])

bsdgpio_module = Extension('_bsdgpio',
                           sources=['_bsdgpio.c'])

hackaeabi_module = Extension('_hackaeabi',
                             sources=['_hackaeabi.c'])

setup(name='iic',
      version='0.1',
      py_modules = [ 'smbus' ],
      ext_modules = [
          bsdiic_module,
          bsdgpio_module,
          #hackaeabi_module,
      ],
      packages = [
          'pybsd',
          'pybsd.iic',
          'pybsd.gpio',
          'RPi',
          'RPi.GPIO',
      ],
      )


#--#
