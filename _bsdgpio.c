/*
 * pybsd/_bsdgpio.c
 *
 * XXX Copyright + License...
 */

#include <Python.h>

#include <sys/gpio.h>

#define HACK(x)		#x
#define STR(x)		HACK(x)

/*********************************************************************/

static
PyObject *
bsdgpio_maxpin(
   PyObject		*self,
   PyObject		*args
   )
{
   int fd;
   if (!PyArg_ParseTuple(args, "i", &fd))
      return NULL;

   int maxpins;
   int err = ioctl(fd, GPIOMAXPIN, &maxpins);
   if (err != 0) {
      PyErr_SetFromErrno(PyExc_IOError);
      return NULL;
   }

   return PyLong_FromLong(maxpins);
}

static
PyObject *
bsdgpio_get_pin_config(
   PyObject		*self,
   PyObject		*args
   )
{
   int fd, pin_number;
   if (!PyArg_ParseTuple(args, "ii", &fd, &pin_number))
      return NULL;

   struct gpio_pin gp;
   memset(&gp, 0, sizeof(gp));
   gp.gp_pin = pin_number;

   int err = ioctl(fd, GPIOGETCONFIG, &gp);
   if (err != 0) {
      PyErr_SetFromErrno(PyExc_IOError);
      return NULL;
   }

   // return ( name, caps, flags )
   PyObject *t = PyTuple_New(3);
   PyTuple_SetItem(t, 0, PyBytes_FromString(gp.gp_name));
   PyTuple_SetItem(t, 1, PyLong_FromLong(gp.gp_caps));
   PyTuple_SetItem(t, 2, PyLong_FromLong(gp.gp_flags));
   return t;
}

static
PyObject *
bsdgpio_set_pin_name(
   PyObject		*self,
   PyObject		*args
   )
{
   int fd, pin_number;
   PyObject *name;
   if (!PyArg_ParseTuple(args, "iiO!", &fd, &pin_number,
			 PyBytes_Type, &name))
      return NULL;

   int namelen = PyBytes_Size(name);
   if ((namelen <= 0) || (namelen >= GPIOMAXNAME)) {
      PyErr_SetString(PyExc_ValueError,
		      "min name length 1, less than " STR(GPIOMAXNAME));
      return NULL;
   }

   struct gpio_pin gp;
   memset(&gp, 0, sizeof(gp));
   gp.gp_pin = pin_number;
   memcpy(gp.gp_name, PyBytes_AsString(name), namelen);

   int err = ioctl(fd, GPIOSETNAME, &gp);
   if (err != 0) {
      PyErr_SetFromErrno(PyExc_IOError);
      return NULL;
   }

   Py_INCREF(Py_None);
   return Py_None;
}

static
PyObject *
bsdgpio_set_pin_flags(
   PyObject		*self,
   PyObject		*args
   )
{
   int fd, pin_number, flags;
   PyObject *name;
   if (!PyArg_ParseTuple(args, "iiI", &fd, &pin_number, &flags))
      return NULL;

   struct gpio_pin gp;
   memset(&gp, 0, sizeof(gp));
   gp.gp_pin = pin_number;
   gp.gp_flags = flags;

   int err = ioctl(fd, GPIOSETCONFIG, &gp);
   if (err != 0) {
      PyErr_SetFromErrno(PyExc_IOError);
      return NULL;
   }

   Py_INCREF(Py_None);
   return Py_None;
}

static
PyObject *
bsdgpio_get_pin_value(
   PyObject		*self,
   PyObject		*args
   )
{
   int fd, pin_number;
   if (!PyArg_ParseTuple(args, "ii", &fd, &pin_number))
      return NULL;

   struct gpio_req gr;
   gr.gp_pin = pin_number;
   gr.gp_value = 0;

   int err = ioctl(fd, GPIOGET, &gr);
   if (err != 0) {
      PyErr_SetFromErrno(PyExc_IOError);
      return NULL;
   }

   PyObject *rv = (gr.gp_value ? Py_True : Py_False);
   Py_INCREF(rv);
   return rv;
}

static
PyObject *
bsdgpio_set_pin_value(
   PyObject		*self,
   PyObject		*args
   )
{
   int fd, pin_number;
   PyObject *high_low;
   if (!PyArg_ParseTuple(args, "iiO", &fd, &pin_number, &high_low))
      return NULL;

   struct gpio_req gr;
   gr.gp_pin = pin_number;
   gr.gp_value = (high_low == Py_True);

   int err = ioctl(fd, GPIOSET, &gr);
   if (err != 0) {
      PyErr_SetFromErrno(PyExc_IOError);
      return NULL;
   }

   Py_INCREF(Py_None);
   return Py_None;
}

static
PyObject *
bsdgpio_toggle_pin(
   PyObject		*self,
   PyObject		*args
   )
{
   int fd, pin_number;
   if (!PyArg_ParseTuple(args, "iiO", &fd, &pin_number))
      return NULL;

   struct gpio_req gr;
   gr.gp_pin = pin_number;
   gr.gp_value = 0;

   int err = ioctl(fd, GPIOTOGGLE, &gr);
   if (err != 0) {
      PyErr_SetFromErrno(PyExc_IOError);
      return NULL;
   }

   Py_INCREF(Py_None);
   return Py_None;
}


static PyMethodDef bsdgpio_methods[] =
{
   {
      "maxpin",
      bsdgpio_maxpin,
      METH_VARARGS,
      "GPIOMAXPIN"
   }, {
      "get_pin_config",
      bsdgpio_get_pin_config,
      METH_VARARGS,
      "GPIOGETCONFIG"
   }, {
      "set_pin_name",
      bsdgpio_set_pin_name,
      METH_VARARGS,
      "GPIOSETNAME"
   }, {
      "set_pin_flags",
      bsdgpio_set_pin_flags,
      METH_VARARGS,
      "GPIOSETCONFIG"
   }, {
      "get_pin_value",
      bsdgpio_get_pin_value,
      METH_VARARGS,
      "GPIOGET"
   }, {
      "set_pin_value",
      bsdgpio_set_pin_value,
      METH_VARARGS,
      "GPIOSET"
   }, {
      "toggle_pin",
      bsdgpio_toggle_pin,
      METH_VARARGS,
      "GPIOTOGGLE"
   }, {
      0
   }
};

/*********************************************************************/

static struct PyModuleDef bsdgpio_module =
{
   PyModuleDef_HEAD_INIT,
   "_bsdgpio",
   NULL,		// m_doc
   -1,			// m_size
   bsdgpio_methods,
   0			// ...
};


PyObject *
PyInit__bsdgpio(void)
{
   PyObject *m = PyModule_Create(&bsdgpio_module);
   if (m == NULL)
      return NULL;

   //PyModule_AddIntConstant(m, "PIN_LOW", GPIO_PIN_LOW);
   //PyModule_AddIntConstant(m, "PIN_HIGH", GPIO_PIN_HIGH);

   PyModule_AddIntConstant(m, "PIN_INPUT", GPIO_PIN_INPUT);

   PyModule_AddIntConstant(m, "PIN_OUTPUT", GPIO_PIN_OUTPUT);
   PyModule_AddIntConstant(m, "PIN_OPENDRAIN", GPIO_PIN_OPENDRAIN);
   PyModule_AddIntConstant(m, "PIN_PUSHPULL", GPIO_PIN_PUSHPULL);
   PyModule_AddIntConstant(m, "PIN_TRISTATE", GPIO_PIN_TRISTATE);

   PyModule_AddIntConstant(m, "PIN_PULLUP", GPIO_PIN_PULLUP);
   PyModule_AddIntConstant(m, "PIN_PULLDOWN", GPIO_PIN_PULLDOWN);

   PyModule_AddIntConstant(m, "PIN_INVIN", GPIO_PIN_INVIN);
   PyModule_AddIntConstant(m, "PIN_INVOUT", GPIO_PIN_INVOUT);


   return m;
}

/**/
