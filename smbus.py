#!python3.6

from pybsd.iic import IIC

class SMBus:

    def __init__(self, bus=None):
        self._iic = IIC()
        if bus is not None:
            self.open(bus)
        return

    def open(self, bus):
        self._iic.open('/dev/iic%i' % bus)
        return

    def set_addres(self, addr):
        self._iic.set_slave_address(addr)
        return #int ioctl status? -or- IOError from errno?

    def write_quick(self, addr):
        XXX

    def read_byte(self, addr):
        XXX

    def write_byte(self, addr, val):
        XXX

    def read_byte_data(self, addr, cmd):
        XXX

    def write_byte_data(self, addr, cmd, val):
        XXX

    # word = 16-bits

    def read_word_data(self, addr, cmd):
        XXX

    def write_word_data(self, addr, cmd, value):
        XXX

    def process_call(self, addr, cmd, val):
        XXX

    def read_block_data(self, addr, cmd):
        XXX

    def write_block_data(self, addr, cmd, val_list):
        XXX

    def block_process_call(self, addr, cmd, val_list):
        XXX

    def read_i2c_block_data(self, addr, cmd, len=32):
        XXX

    def write_i2c_block_data(self, addr, cmd, val_list):
        out = bytearray(1 + len(val_list))
        out[0] = cmd
        out[1:] = val_list
        self._iic.write(bytes(out), slave_address=addr)
        return

    #@attribute get_pec, set_pec (bool)

#--#
