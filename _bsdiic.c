/*
 * pybsd/_bsdiic.c
 *
 * XXX Copyright + License...
 */

#include <Python.h>

#include <dev/iicbus/iic.h>

// XXX struct iic_msg { u_int16_t len; }
#define BSDIIC_MAX_MSGLEN	(16385)

/*********************************************************************/

static
PyObject *
bsdiic_reset_card(
   PyObject		*self,
   PyObject		*args
   )
{
   int fd;
   if (!PyArg_ParseTuple(args, "i", &fd))
      return NULL;

   int err = ioctl(fd, I2CRSTCARD, NULL);
   if (err != 0) {
      PyErr_SetFromErrno(PyExc_IOError);
      return NULL;
   }

   Py_INCREF(Py_None);
   return Py_None;
}


static
PyObject *
bsdiic_set_slave_address(
   PyObject		*self,
   PyObject		*args
   )
{
   int fd, addr;
   if (!PyArg_ParseTuple(args, "ii", &fd, &addr))
      return NULL;

   uint8_t sa = (addr << 1);
   int err = ioctl(fd, I2CSADDR, &sa);
   if (err != 0) {
      PyErr_SetFromErrno(PyExc_IOError);
      return NULL;
   }

   Py_INCREF(Py_None);
   return Py_None;
}


static
PyObject *
bsdiic_read(
   PyObject		*self,
   PyObject		*args
   )
{
   int fd, addr, nbytes;
   PyObject *obj;
   if (!PyArg_ParseTuple(args, "iii", &fd, &addr, &nbytes))
      return NULL;

   if ((nbytes <= 0) || (nbytes > BSDIIC_MAX_MSGLEN)) {
      PyErr_SetString(PyExc_ValueError,
		      "min length 1, max length 64k-1");
      return NULL;
   }

   PyObject *data = PyBytes_FromStringAndSize(NULL, nbytes);
   if (data == NULL)
      return NULL;

   struct iic_msg msg;
   msg.slave = (addr << 1);	// XXX As seen on analyzer.
   msg.flags = IIC_M_RD;
   msg.len = PyBytes_Size(obj);
   msg.buf = (uint8_t *)PyBytes_AsString(obj);

   struct iic_rdwr_data rdwr;
   rdwr.msgs = &msg;
   rdwr.nmsgs = 1;

   int err = ioctl(fd, I2CRDWR, &rdwr);
   if (err != 0) {
      Py_DECREF(data);
      return PyErr_SetFromErrno(PyExc_OSError);
   }

   return data;
}


static
PyObject *
bsdiic_write(
   PyObject		*self,
   PyObject		*args
   )
{
   int fd, addr;
   PyObject *obj;
   if (!PyArg_ParseTuple(args, "iiO", &fd, &addr, &obj))
      return NULL;

   if (!PyBytes_Check(obj) || (PyBytes_Size(obj) > 16385)) {
      PyErr_SetString(PyExc_ValueError,
		      "data must be bytes, max length 64k-1");
      return NULL;
   }

   struct iic_msg msg;
   msg.slave = (addr << 1);	// XXX As seen on analyzer.
   msg.flags = IIC_M_WR;
   msg.len = PyBytes_Size(obj);
   msg.buf = (uint8_t *)PyBytes_AsString(obj);

   struct iic_rdwr_data rdwr;
   rdwr.msgs = &msg;
   rdwr.nmsgs = 1;

   int err = ioctl(fd, I2CRDWR, &rdwr);
   if (err != 0)
      return PyErr_SetFromErrno(PyExc_OSError);

   Py_INCREF(Py_None);
   return Py_None;
}


static PyMethodDef bsdiic_methods[] =
{
   {
      "reset_card",
      bsdiic_reset_card,
      METH_VARARGS,
      "I2CRSTCARD"
   }, {
      "set_slave_address",
      bsdiic_set_slave_address,
      METH_VARARGS,
      "I2CSADDR"
   }, {
      "read",
      bsdiic_read,
      METH_VARARGS,
      "I2CRDWR - read"
   }, {
      "write",
      bsdiic_write,
      METH_VARARGS,
      "I2CRDWR - write"
   }, {
      0
   }
};

/*********************************************************************/

static struct PyModuleDef bsdiic_module =
{
   PyModuleDef_HEAD_INIT,
   "_bsdiic",
   NULL,		// m_doc
   -1,			// m_size
   bsdiic_methods,
   0			// ...
};


PyObject *
PyInit__bsdiic(void)
{
   PyObject *m = PyModule_Create(&bsdiic_module);
   if (m == NULL)
      return NULL;

   // Exception class?

   return m;
}

/**/
